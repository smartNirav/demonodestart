'use strict';

var helpers      = require('helpers'),
    httpResponse = require(process.cwd() + '/config/httpStatus'),
    stripe       = require('stripe')('sk_test_UXvhOamA5o7NjeWVM3iXBMYl'),
    fs           = require('fs'),
    fileUpload   = require('express-fileupload'),
    jwt         = require('jsonwebtoken'),
    jwtConf     = require(process.cwd() + '/config/jwt');

module.exports = function(app) {

    var authController = require('../app/auth/AuthController'),
        userController = require('../app/components/User/UserController');

    //home route
    app.route('/')
        .get(function (req, res) {
            helpers.createResponse(res, httpResponse.SUCCESS,
                "This is API based project and no view available.",
                { "data" : "Data here" }
            );
        });

    //Signup Route
    app.route('/users')
        .get(userController.index);

    app.route('/signup')
        .post(authController.signUp);

    app.route('/login')
        .post(authController.login);

    app.use(function (req, res, next) {
        var token = req.headers['authorization']; // Parse token only from header

        //console.log(req.authorization);
        if (token) {
            token = token.split('Bearer ')[1]
            jwt.verify(token, jwtConf.secret, function(err, decoded) {
                if (err)
                    return res.json({success : false, message : 'Failed to authenticate token.'});
                else {
                    req.decoded = decoded;
                    next();
                }
            });
        } else {
            helpers.createResponse(res, httpResponse.UNAUTHORIZED,
                "Request parameter missing.",
                { "error" : "Please provide the access token." }
            );
        }
    });

    app.route('/users/:userId')
        .get(userController.show);

    //file upload - multipart
    app.route('/fileUpload')
        .post(function (req, res) {
            if (req.files)
            {
                //file is uploaded
                if (req.files.thumbnail.mimetype === 'image/svg+xml' ||
                    req.files.thumbnail.mimetype === 'image/gif' ||
                    req.files.thumbnail.mimetype === 'image/jpeg' ||
                    req.files.thumbnail.mimetype === 'image/png' ) {

                    //uploaded file is a valid image file, proceed for your operation
                    res.send(req.files.thumbnail.mimetype);
                }
                else {
                    helpers.createResponse(res, httpResponse.BAD_REQUEST,
                        "The thumbnail must be a valid image file",
                        { "data" : "Invalid file type" }
                    );
                }
            }
            // update profile details other than image
        });


    //stripe test
    app.route('/charge')
        .post(function (req, res) {

            var token = stripe.tokens.create({
                card : {
                    "number"    : "4242424242424242",
                    "exp_month" : 12,
                    "exp_year"  : 2018,
                    "cvc"       : "123"
                }
            }, function (err, token) {
                if (err) {
                    helpers.createResponse(res, httpResponse.SERVER_ERROR,
                        "There is some error creating token",
                        { "data" : "Server Error" }
                    );
                }
                else
                {
                    var stripeToken = token.id;
                    var amount      = 100;

                    stripe.charges.create({
                        source      : stripeToken,
                        currency    : 'usd',
                        amount      : amount,
                        description : "Charge from techne app"
                    }, function (err, charge) {
                        if (err) {
                            helpers.createResponse(res, httpResponse.SERVER_ERROR,
                                "There is some error creating charges",
                                { "data" : "Server Error" }
                            );
                        } else {
                            helpers.createResponse(res, httpResponse.SUCCESS,
                                "Your transaction has been processed successfully !",
                                { "data" : "Payment Received !" }
                            );
                        }
                    });
                }
            });
        });
};