'use strict';

var User            =  require('../components/User/User'),
    mongoose        = require('mongoose'),
    bcrypt          = require('bcrypt'),
    User            = mongoose.model('User'),
    jwt             = require('jsonwebtoken'),
    jwtConf         = require('../../config/jwt'),
    helpers         = require('helpers'),
    httpResponse    = require('httpStatus'),
    mail            = require('../mailer/mail'),
    fs              = require('fs');

exports.signUp = function (req, res) {
    User.findOne({
        email : req.body.email
    }, function (err, user) {
        if (err) {
            helpers.createResponse(res, httpResponse.SERVER_ERROR,
                "Error during Sign Up list.",
                { "error" : "Server Error"}
            );
        }
        if (!user) {
            var newUser = new User({
                name        : req.body.name,
                email       : req.body.email,
                password    : bcrypt.hashSync(req.body.password, 10),
                mobileNumber: req.body.mobileNumber,
            });
            newUser.save(function (err, user) {
                if (err) {
                    helpers.createResponse(res, httpResponse.SERVER_ERROR,
                        "Error during User list.",
                        { "error" : "Server Error"}
                    );
                }

                //read the welcome template

                var filePath = process.cwd() + '/app/mailer/templates/welcomeGreet.txt';
                fs.readFile(filePath, "utf-8", function (err, data) {
                    if (err)
                        res.json(err);
                    else
                    {
                        var welcomeTemplate = data;

                        //replace the name now
                        welcomeTemplate = welcomeTemplate.replace('#name', req.body.name);

                        // setup email data with unicode symbols
                        var mailOptions = {
                            from: 'Nirav S. Majethiya', // optional
                            to: req.body.email, // list of receivers
                            subject: 'Registration', // Subject line
                            text: 'Registered', // plain text body
                            html: welcomeTemplate // html body
                        };

                        // send mail with defined transport object
                        mail.transporter.sendMail(mailOptions, (error, info) => {
                            if (error) {
                                return console.log(error);
                            }
                            console.log('Message %s sent: %s', info.messageId, info.response);
                        });
                    }
                });

                helpers.createResponse(res, httpResponse.SUCCESS,
                    "Congrats! You have successfully registered.",
                    { "token" : generateToken(user), "user" : user }
                );
            });
        }
        else {
            helpers.createResponse(res, httpResponse.UNAUTHORIZED,
                "The email has already been taken.",
                { "error" : "The email has already been taken."}
            );
        }
    });
};

exports.login = function (req, res) {

    //return res.json(req.body);
    //find the user if already exists
    var user = User.findOne({
        email : req.body.email
    }, function (err, user) {
        if (err) {
            helpers.createResponse(res, httpResponse.SERVER_ERROR,
                "Error during login.",
                {"error": "Server Error"}
            );
        }
        if (!user) {
            helpers.createResponse(res, httpResponse.UNAUTHORIZED,
                "Invalid user",
                {"error": "User not found"}
            );
        }
        else
        {
            if (bcrypt.compareSync(req.body.password, user.password)) {
                helpers.createResponse(res, httpResponse.SUCCESS,
                    "Login Successful !",
                    { "token" : generateToken(user), "user" : user }
                );
            }
            else {
                helpers.createResponse(res, httpResponse.UNAUTHORIZED,
                    "Invalid password",
                    { "error" : "Inncorrect password"}
                );
            }
        }

    });
};

function generateToken(user) {
    return jwt.sign(user, jwtConf.secret, {
        expiresIn: 10080
    });
}

//set user info from request
function setUserInfo(request) {
    return {
        id       : request._id,
        name      : request.name,
        email	  : request.email,
        isEnable  : request.isEnable,
        created_at: request.created_at,
    };
}