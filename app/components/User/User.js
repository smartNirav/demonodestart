'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    name : {
        required : 'The name field is required',
        type     : String
    },
    email : {
        required : 'The email field is required',
        type     : String,
        min      : [3, 'The email must be at least 3 characters'],
        max      : [100, 'The email may not be greater than 100 characters']
        //unique   : 'The email has already been taken'
    },
    password : {
        required : 'The password field is required',
        type     : 'String',
        min      : [3, 'The password must be at least 3 characters']
    },
    mobileNumber : {
        type     : Number
    },
    isEnable : {
        type     : Number,
        default  : 1
    },
    created_at : {
        type : Date,
        default : Date.now
    },
    updated_at : {
        type : Date,
        default : Date.now
    }
});

module.exports = mongoose.model('User', userSchema);