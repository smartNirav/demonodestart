'use strict';

var User     = require('./User'),
    mongoose = require('mongoose'),
    User     = mongoose.model('User'),
    helpers      = require('helpers'),
    httpResponse = require('httpStatus');

exports.index = function (req, res) {
    User.find({}, function(err, users) {
       if (err)
       {
           helpers.createResponse(res, httpResponse.SERVER_ERROR,
               "Error during User list.",
               { "error" : "Server Error"}
           );
       }
       helpers.createResponse(res, httpResponse.SUCCESS,
            "User list",
            { "data" : users }
        );

    });
};

exports.show = function (req, res) {
    User.findById(req.params.userId, function (err, user) {
        if (err)
        {
            helpers.createResponse(res, httpResponse.SERVER_ERROR,
                "Error during User list.",
                { "error" : "Server Error"}
            );
        }
        helpers.createResponse(res, httpResponse.SUCCESS,
            "User list",
            { "data" : user }
        );
    });
};