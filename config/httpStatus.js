const   SUCCESS             = 200,
        ACCEPTED             = 202,
        BAD_REQUEST         = 400,
        UNAUTHORIZED        = 401,
        FORBIDDEN           = 403,
        NOT_FOUND           = 404,
        METHOD_NOT_ALLOWED  = 405,
        UNPROCESSED         = 422,
        SERVER_ERROR        = 500;

module.exports = {
    SUCCESS             : SUCCESS,
    ACCEPTED            : ACCEPTED,
    BAD_REQUEST         : BAD_REQUEST,
    UNAUTHORIZED        : UNAUTHORIZED,
    FORBIDDEN           : FORBIDDEN,
    NOT_FOUND           : NOT_FOUND,
    METHOD_NOT_ALLOWED  : METHOD_NOT_ALLOWED,
    UNPROCESSED         : UNPROCESSED,
    SERVER_ERROR        : SERVER_ERROR
};