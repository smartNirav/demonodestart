var express     = require('express'),
    app         = express(),
    port        = process.env.PORT || 3000,
    mongoose    = require('mongoose'),
    jwt         = require('jsonwebtoken'),
    jwtConf     = require('./config/jwt'),
    bodyParser  = require('body-parser'),
    fileUpload   = require('express-fileupload');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/DemoNodeStart');

app.use(bodyParser.urlencoded({extended : false})); // extended false will accept only string and array
app.use(bodyParser.json());
app.use(fileUpload());

var routes = require('./routes/api.js');
routes(app);

app.listen(port);
console.log('RESTful API server started on : ' + port);

app.use(function (req, res) {
    res.status(404).send({url : req.originalUrl + ' not found'})
});